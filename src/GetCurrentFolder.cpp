#include "GetCurrentFolder.h"

// Какие-то библиоткеи не используются, нужно будет почистить
#include <iostream>
#include <string.h>
#include <locale.h>
#include <ctime>
#include <cstdlib>

#include <vector>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <sstream>

#include <fstream>
#include <math.h>
#include <algorithm>
#include <set>
#include <memory>

#include <dirent.h>
#include <stdio.h>
#include <unistd.h>

std::string getCurrPath() {
    char* path = new char[PATH_MAX + 1];

    ssize_t len = readlink("/proc/self/exe", path, PATH_MAX);
    path[len]   = 0;
    char* p = strrchr(path, '/');
    if (p) *(p + 1)  = 0;
    else path[0]    = 0;

    std::string spath(path);
    delete[] path;

    return spath;
}

bool isFIleContainString(const std::string &name, const std::string &str) {
    std::ifstream fin(name.c_str());
    if (!fin) {
        std::cout << "NOT FOUND FILE\n";
        return -1;
    }

    std::string buf;
    while (getline(fin, buf)) {
        if (buf == str) {
            fin.close();
            return true;
        }
    }
    fin.close();
    return false;
}

std::string correctString(std::string str) {
    //std::cout << "str: " << "\"" << str << "\"" << std::endl;
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    while (*(str.begin() - 1) == ' ') {
        std::cout << *(str.begin() - 1) << std::endl;
        str.erase((str.begin() - 1));
    }
    while (*(str.end() - 1) == ' ') {
        std::cout << *(str.end() - 1) << std::endl;
        str.erase((str.end() - 1));
    }

    return str;
}