#include <iostream>
#include <string.h>
#include <locale.h>
#include <ctime>
#include <cstdlib>

#include <vector>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <sstream>

#include <fstream>
#include <math.h>
#include <algorithm>
#include <set>
#include <memory>

#include <dirent.h>
#include <stdio.h>
#include <unistd.h>


#include "BayesClassifier.h"
#include "LexerStem.h"
#include "LexerMyStem.h"
#include "types.h"

#include "GetCurrentFolder.h"
#include "BayesLearn.h"
#include "ChatBot.h"

#define DELTA_FOR_ADDED 0.2

void BayerLearner::Learn(std::string config, std::string classPath) {
    // config = getCurrPath() + config;
    // classPath = getCurrPath() + classPath;

    lexer * Lexer = new LexerStem();

    DIR *dir;
    struct dirent *ent;

    WordList words_all;
    ClassifierList classifier;
    ClassifierList::iterator words = classifier.end();


    if ((dir = opendir(classPath.c_str())) == NULL) {
        std::cout << "err" << std::endl;
        return;
    }
    // Итерируем по всем файлам в папке
    while ((ent = readdir(dir)) != NULL) {
        // Пропускаем . и ..
        if (ent->d_name[0] == '.') {
            continue;
        }

        std::string fileN = classPath + "/" + std::string(ent->d_name);
        std::ifstream fin(fileN.c_str());
        if (!fin.is_open()) {
            std::cout << "cant open file: " << fileN << std::endl;
        }
        int cl = atoi(ent->d_name); // Класс определяем по имени файла
        // Считываем весь файл
        while (!fin.eof()) {
            std::string buf;
            std::getline(fin, buf);
            buf = correctString(buf);
            icu::UnicodeString ucs = icu::UnicodeString::fromUTF8(buf.c_str());

            if (classifier.find(cl) == classifier.end()) {
                ClassifierStat stat;
                stat.docs_count = 0;
                classifier[cl] = stat;
            }
            words = classifier.find(cl);
            words->second.docs_count ++;
            Lexer->parse_begin();

            assert(words != classifier.end());

            ucs.toLower();
            Lexer->parse(ucs);
            Lexer->word_stat(words->second.words);
        }
        fin.close();
    }
    closedir(dir);

    Lexer->parse_end();
    Lexer->word_stat(words->second.words);

    BayesClassifier bayes;
    bayes.setStats(classifier);

    std::ofstream ofs(config.c_str());
    bayes.saveToStream(ofs);
    ofs.close();

    delete  Lexer ;
}

void BayerLearner::AutoLearn(std::string config, std::string dialog , std::string classFolder, std::string answerFolder) {
    std::cout << "Learn file" << dialog << std::endl;
    // std::cout << "Files: " << filenumb << std::endl;

    std::ifstream ilearn(dialog.c_str());
    if (!ilearn) {
        std::cout << "NOT FOUND FILE\n";
    }
    int filenumb = 0;

    // Считаем файлы
    DIR *dir;
    struct dirent *ent;

    if ((dir = opendir(classFolder.c_str())) == NULL) {
        std::cout << "err" << std::endl;
    }
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_name[0] != '.') {
            filenumb++;
        }
    }
    //

    std::string message1 = "";
    std::string message2 = "";
    int counter = 0;
    std::cout << "START OF CLASSIFICATON" << std::endl;
    std::cout << "Learn file" << dialog << std::endl;
    std::cout << "Files: " << filenumb << std::endl;

    ChatBot bot(config, answerFolder);

    // bool work = true;
    // std::cout << ilearn.eof() << std::endl;

    while (!ilearn.eof()) {
        counter++;
        std::string buf;
        while (getline(ilearn, buf) && buf == "") {
            // std::cout << "buf: " << buf << std::endl;
        }
        message1 = buf + " ";
        while (getline(ilearn, buf) && buf != "") {
            // std::cout << "buf: " << buf << std::endl;
            message1 += buf + " ";
        }
        while (getline(ilearn, buf) && buf == "") {
            // std::cout << "buf: " << buf << std::endl;
        }
        message2 = buf + " ";
        while (getline(ilearn, buf) && buf != "") {
            // std::cout << "buf: " << buf << std::endl;
            message2 += buf + " ";
        }

        message1 = correctString(message1);
        message2 = correctString(message2);

        std::cout << "m1: " << "\"" << message1 << "\"" << std::endl;
        std::cout << "m2: " << "\"" << message2 << "\"" << std::endl;

        ClassifierA answer = bot.classifier(config, message1);
        ClassifierA::iterator max = std::max_element(answer.begin(), answer.end(),
        [](const std::pair<unsigned int, float>& p1, const std::pair<unsigned int, float>& p2) {
            return p1.second < p2.second;
        });
        ClassifierA::iterator min = std::min_element(answer.begin(), answer.end(),
        [](const std::pair<unsigned int, float>& p1, const std::pair<unsigned int, float>& p2) {
            return p1.second < p2.second;
        });
        std::cout << "DELTA: " << max->second - min->second << std::endl;
        if (max->second - min->second < DELTA_FOR_ADDED) { // Надеюсь тут будет float
            std::ofstream ofs(classFolder + "/" + std::to_string(filenumb));
            ofs << message1 << std::endl;
            std::cout << "Added: " << classFolder <<  "/" << filenumb << std::endl;
            ofs.close();

            ofs.open(answerFolder +  "/" + std::to_string(filenumb));
            ofs << message2 << std::endl;
            std::cout << "Added: " << answerFolder <<  "/" << filenumb << std::endl;
            ofs.close();
            BayerLearner::Learn(config, classFolder);
            filenumb++;
        } else {
            std::ofstream ofs;
            if (!isFIleContainString(classFolder +  "/" + std::to_string(max->first), message1)) {
                ofs.open(classFolder +  "/" + std::to_string(max->first), std::ofstream::app);
                ofs << message1 << std::endl;
                ofs.close();
                std::cout << "Append: " << classFolder <<  "/" << max->first << std::endl;
            }
            if (!isFIleContainString(answerFolder +  "/" + std::to_string(max->first), message2)) {
                ofs.open(answerFolder +  "/" + std::to_string(max->first), std::ofstream::app);
                ofs << message2 << std::endl;
                ofs.close();
                std::cout << "Append: " << answerFolder <<  "/" << max->first << std::endl;
            }
            BayerLearner::Learn(config, classFolder);
        }
        std::cout << std::endl;
    }

    ilearn.close();
}