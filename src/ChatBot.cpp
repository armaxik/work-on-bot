#include <string>
#include <locale.h>
#include <ctime>
#include <cstdlib>

#include <vector>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <sstream>

#include <fstream>
#include <math.h>
#include <algorithm>
#include <set>
#include <memory>

#include <dirent.h>
#include <stdio.h>
#include <unistd.h>


#include "BayesClassifier.h"
#include "LexerStem.h"
#include "LexerMyStem.h"

#include "ChatBot.h"
#include "GetCurrentFolder.h"

ChatBot::ChatBot(): config("NULL"), answFolder("NULL") {

}

ChatBot::ChatBot(const std::string& conf, const std::string& answF): config(conf), answFolder(answF) {

}

std::string ChatBot::Answer(const std::string& str) {
    std::string workStr = correctString(str);
    if (config == "NULL") {
        return std::string("ERROR");
    }
    ClassifierA answer = classifier(config, workStr);  // Получаем список вероятностей распределения строки по классам
    // Находим класс, к которому вероятнее принадледит строка

    // Для отладки
    /*for (auto it = answer.begin(); it != answer.end(); it++) {
        std::cout << it->first << " " << it->second << std::endl;
    }*/

    ClassifierA::iterator max = std::max_element(answer.begin(), answer.end(),
    [](const std::pair<unsigned int, float>& p1, const std::pair<unsigned int, float>& p2) {
        return p1.second < p2.second;
    });

    return getAnswer(max->first);
}

std::string ChatBot::getAnswer(ssize_t cl) {
    if (config == "NULL") {
        return std::string("ERROR");
    }

    std::stringstream strP;
    strP << answFolder << "/" << std::string(std::to_string(cl));
    std::ifstream ifs(strP.str());

    if (!ifs.is_open()) {
        return std::string("NOT FOUND FILE: ") + strP.str();
    }
    // Подсчитываем строки в файле
    std::string answ;
    int fnum = 0;
    while (std::getline(ifs, answ)) {
        fnum++;
    }
    // Возвращаемся вначало файла
    ifs.clear();
    ifs.seekg(0, std::ios::beg);
    // Выбираем случайную строку
    std::srand(std::time(nullptr)); // use current time as seed for random generator
    int rstr = std::rand() % fnum;
    for (int i = -1; i < rstr; i++) {
        getline(ifs, answ);
    }

    ifs.close();

    return answ;
}

ClassifierA ChatBot::classifier(const std::string& config, const std::string& input) {
    std::string workStr = correctString(input);
    lexer * Lexer = new LexerStem();

    BayesClassifier bayes;
    std::ifstream ifs(config.c_str());

    if (!ifs) {
        std::cerr << "Файл не найден: " << config << std::endl;
        exit(1);
    }

    bayes.loadFromStream(ifs);
    ifs.close();

    ClassifierList classifier;
    WordsStat words;

    Lexer->parse_begin();

    icu::UnicodeString ucs = icu::UnicodeString::fromUTF8(workStr);

    ucs.toLower();

    Lexer->parse(ucs);
    Lexer->word_stat(words);

    Lexer->parse_end();
    Lexer->word_stat(words);

    ClassifierP p;

    ClassifierP answer = bayes.classify(words);

    for (ClassifierP::const_iterator i = answer.begin(); i != answer.end(); i ++) {
        float sum = 0;
        for (ClassifierP::const_iterator j = answer.begin(); j != answer.end(); j++ ) {
            sum += exp( j->second - i->second );
        }
        p[i->first] = 1 / sum;
    }

    words.clear();

    ClassifierA myAnswer;

    for (ClassifierP::iterator item = answer.begin(); item != answer.end(); item++) {
        myAnswer.insert(std::pair<unsigned int, float>(item->first, p[item->first]));
    }

    delete Lexer;
    return myAnswer;
}
