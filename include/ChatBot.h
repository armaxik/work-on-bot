#ifndef CHATBOT_H_
#define CHATBOT_H_

#include <iostream>

// Подразумевается, что все пути абсолютные
class ChatBot {
public:
    ChatBot();
    ChatBot(const std::string& conf, const std::string& answF);
    std::string Answer(const std::string& str);
    ClassifierA classifier(const std::string& config, const std::string& input);
private:
    std::string config;
    std::string answFolder;
    std::string getAnswer(ssize_t cl);
};

#endif  // CHATBOT_H_