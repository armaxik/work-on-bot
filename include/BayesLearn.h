#ifndef BAYESLEARN_H
#define BAYESLEARN_H

#include <iostream>

// Подразумевается, что все пути абсолютные
namespace BayerLearner {
    void Learn(std::string config, std::string classFolder);
    void AutoLearn(std::string config, std::string dialog ,std::string classFolder, std::string answerFolder);
}

#endif  // BAYESLEARN_H