#ifndef GET_CURRENT_FOLDER_H
#define GET_CURRENT_FOLDER_H

#include <iostream>

std::string getCurrPath();
bool isFIleContainString(const std::string &name, const std::string &str);
std::string correctString(std::string str);

#endif  // GET_CURRENT_FOLDER_H