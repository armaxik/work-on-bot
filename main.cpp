
#include <iostream>
#include <string.h>
#include <locale.h>
#include <ctime>
#include <cstdlib>

#include <vector>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <sstream>

#include <fstream>
#include <math.h>
#include <algorithm>
#include <set>
#include <memory>

#include <dirent.h>
#include <stdio.h>
#include <unistd.h>


#include "BayesClassifier.h"
#include "LexerStem.h"
#include "LexerMyStem.h"
#include "ChatBot.h"
#include "BayesLearn.h"
#include "GetCurrentFolder.h"


using namespace std;

// C [конфиг] [ответы]
// L [конфиг] [папка с классами]
// LA [конфиг] [файл с диалогом] [папка с классами] [папка с ответами]

int main(int argc, char** argv) {
    if (!setlocale(LC_ALL, "ru_RU.utf8")) {
        if (!setlocale(LC_ALL, "russian")) {
            cerr << "Не могу установить русскую локаль: ru_RU.utf8";
            return 1;
        }
    }

    if (argc < 2) {
        cout << "Err" << endl;
        return -1;
    }

    string mode(argv[1]);
    string config;
    if (argc > 2) config = getCurrPath() + string(argv[2]);

    if (mode == "C") {
        string answ = getCurrPath() + string(argv[3]);
        ChatBot bot((config), answ);
        string qstr;
        cout << "Enter string: ";
        getline(cin, qstr);
        cout << bot.Answer(qstr) << endl;
    } else if (mode == "L") {
        string classFolder = getCurrPath() + string(argv[3]);
        BayerLearner::Learn(config, classFolder);
        cout << "Learned succsesful" << endl;
    } else if (mode == "AL") {
        string dialog = getCurrPath() + string(argv[3]);
        string classFolder = getCurrPath() + string(argv[4]);
        string answrFolder = getCurrPath() + string(argv[5]);
        BayerLearner::AutoLearn(config, dialog, classFolder, answrFolder);
        cout << "Learned succsesful" << endl;
    } else if (mode == "H") {
        cout << "C [конфиг] [ответы]\n"
             "L [конфиг] [папка с классами]\n"
             "LA [конфиг] [файл с диалогом] [папка с классами] [папка с ответами]\n";
    }

}
